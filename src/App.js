import {useState, useEffect} from "react";
import { BrowserRouter as Router, Routes, Route, Switch } from "react-router-dom";
import { UserProvider } from "./UserContext";

import AppNavbar from "./components/AppNavbar";

import Home from "./pages/Home";
import Registration from "./pages/Registration";
import Login from "./pages/Login";
import Logout from "./pages/Logout";
import Product from "./pages/Product";
import ProductView from "./pages/ProductView";
import AdminDashboard from "./pages/AdminDashboard";



import {Container} from "react-bootstrap";
import './App.css';

function App() {

  const [user, setUser] = useState({
    id: null,
    isAdmin: null
  })


  const unsetUser = () =>{
    localStorage.clear();
  }

  useEffect(() =>{
    
  }, [user])

  useEffect(() =>{
    fetch(`http://localhost:4000/users/details`,{
      headers:{
        Authorization: `Bearer ${localStorage.getItem("token")}`
      }
    })
    .then(res => res.json())
    .then(data => {
      console.log(data);

      if(typeof data._id !== "undefined"){
          setUser({
            //undefined
            id: data._id,
            isAdmin: data.isAdmin
          })
      }
      
      else{
          setUser({
              id: null,
              isAdmin: null
            })
      }
      
    })
  }, [])

  return (

    <UserProvider value={{user, setUser, unsetUser}}>
        <Router>
          <AppNavbar />
          <Container fluid>
              
              <Routes>
                  <Route exact path ="/" element={<Home />} />
                  <Route exact path ="/product" element={<Product />} />
                  <Route exact path ="/productview/:productId" element={<ProductView />} />
                  <Route exact path ="/admin" element={<AdminDashboard />} />
                  <Route exact path ="/registration" element={<Registration />} />
                  <Route exact path ="/login" element={<Login />} />
                  <Route exact path ="/logout" element={<Logout />} />
              </Routes>
          </Container>
        </Router>
    </UserProvider>

  );
}

export default App;
