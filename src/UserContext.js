import React from "react";

// Creating the Context Object
const UserContext = React.createContext();

export const UserProvider = UserContext.Provider;

export default UserContext;
