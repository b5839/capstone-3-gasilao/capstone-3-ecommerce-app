import { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import {Card, CardGroup, Button, Container, Row, Col} from "react-bootstrap";

export default function ProductCard({productProp}) {
  
  const {_id, name, description, price, stocks} = productProp;

  return (
  	<Col lg={3}>
  	<Container>
  			
		    <CardGroup className= "d-block ">
		      <Card className= "p-3 my-3" style={{ width: '23rem' }}>
		        <Card.Img variant="top" src="" />
		        <Card.Body>
		          <Card.Title>
		          {name}
		          </Card.Title>
		          <Card.Subtitle>
		          	{description}
		          </Card.Subtitle>
		          <Card.Subtitle>Price:</Card.Subtitle>
		          <Card.Text>
		           {price}
		          </Card.Text>
		          <Card.Text>
		           Stocks: {stocks}
		          </Card.Text>
		          <Button as={Link} to={`/productview/${_id}`} variant="primary">Details</Button>
		        </Card.Body>
		      </Card>
		    </CardGroup>
	</Container>
		</Col>

  )
}
