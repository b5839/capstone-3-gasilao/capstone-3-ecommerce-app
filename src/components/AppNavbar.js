import { Link } from "react-router-dom";
import {Navbar, Nav, Container, NavDropdown} from "react-bootstrap";

export default function AppNavBar(){

	return(
		<Navbar bg="light" expand="lg">
	      <Container>
	        <Navbar.Brand>
          	<img
              src="./jklogo.png"
              width="50"
              height="50"
              className="d-inline-block align-top"
            />
          </Navbar.Brand>
	        <Navbar.Toggle aria-controls="basic-navbar-nav" />
	        <Navbar.Collapse id="basic-navbar-nav">
	          <Nav className="ms-auto" defaultActiveKey="/">
	            <Nav.Link as={Link} to="/" eventKey="/">Home</Nav.Link>
	            

	            {	<>
	            	<Nav.Link as={Link} to="/logout" eventKey="/logout">Logout</Nav.Link>
	            	<Nav.Link as={Link} to="/login" eventKey="/login">Login</Nav.Link>
		            <Nav.Link as={Link} to="/registration" eventKey="/registration">Register</Nav.Link>
		            <Nav.Link as={Link} to="/product" eventKey="/product">Product</Nav.Link>
	            	</>
	            }
	          </Nav>
	        </Navbar.Collapse>
	      </Container>
	    </Navbar>
	)
}