import {useEffect, useState} from "react";
import {Row} from "react-bootstrap";

import ProductCard from "../components/ProductCard";

export default function Product(){

    //State tha will be used to store the courses retrieved form the database
     const [products, setProduct] = useState([]);

    //Retrieve the courses from the database upon initial render of the Product component.
    useEffect(() =>{
        fetch(`http://localhost:4000/products/retrieve-product`)
        .then(res => res.json())
        .then(data => {
            setProduct(data.map(product =>{

                return(
                    <ProductCard key={product._id} productProp={product} />
                )
            }))
        })
    },[])
    return(
        <>
            <h1 className = "my-5 text-center">Products</h1>
            <Row>{products}</Row>
            
            
        </>
    )
}
