import Banner from "../components/Banner";

export default function Home(){
	const data = {
		title: "JK Home Appliances Online Shop",
		content: "We got what you need for your home",
		destination: "/products",
		label: "View Products"
	};



	return(
		<>
		<Banner data={data}/>
		</>
		
	)
}