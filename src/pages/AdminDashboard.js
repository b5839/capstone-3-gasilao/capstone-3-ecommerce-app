import { useState, useEffect, useContext} from "react"
import {Table, Button, Container,Row, Modal, Form} from 'react-bootstrap';
import { Navigate, Link, useParams, useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2';

export default function AdminDashboard() {

  const [show, setShow] = useState(false);
  const [name, setName] = useState("");
  const [description, setDescription] = useState("");
  const [price, setPrice] = useState(0);
  const [stocks, setStocks] = useState(0);
  const [productId, setProductId] = useState();
  const navigate = useNavigate();

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  const [updateModal, setUpdateModal] = useState(false);
  const updateClose = () => setUpdateModal(false);
  const updateShow = () => setUpdateModal(true);
  
  const update = async (id) =>{
   
    setProductId(id);


fetch(`http://localhost:4000/products/${id}`)
        .then(res => res.json())
        .then(data => {
            setName(data.name);
            setDescription(data.description);
            setPrice(data.price);
            setStocks(data.stocks);
        })
     
    updateShow();

 }

  function addNewProduct(e) {
    e.preventDefault();
     fetch(`http://localhost:4000/products/add-product`, {
        method: "POST",
        headers: {
        "Content-Type": "application/json",
        "Authorization": `Bearer ${localStorage.getItem('token')}`
      },
      body: JSON.stringify({
          name: name,
          description: description,
          price: price,
          stocks: stocks
      })
      })
      .then(res => res.json())
      .then(data => {


        if(data){
          Swal.fire({
              title: "Product succesfully Added",
              icon: "success",
              text: `${name} is now added`
          });
          viewAllProduct();
        
        }
        else{
          Swal.fire({
              title: "Error!",
              icon: "error",
              text: `Something went wrong. Please try again later!`
          });
        }

      })

      // Clear input fields
      setName("");
      setDescription("");
      setPrice(0);
      setStocks(0);

  }

  function updateProduct(e) {

    e.preventDefault();
    
    fetch(`http://localhost:4000/products/update-product/${productId}`,{
      method: "PUT",
      headers: {
      "Content-Type": "application/json",
      "Authorization": `Bearer ${localStorage.getItem('token')}`  
      },
      body: JSON.stringify({
      name: name,
      description: description,
      price: price,
      stocks: stocks
    })
    })
    .then(res => res.json())
    .then(data =>{

      if(data){
        Swal.fire({
          title: "Product succesfully Updated",
          icon: "success",
        });
        viewAllProduct();
      }
      else{
        Swal.fire({
          title: "Error!",
          icon: "error",
          text: `Something went wrong. Please try again later!`
        })
      }
    })
    // Clear input fields
      setName("");
      setDescription("");
      setPrice(0);
      setStocks(0);
    updateClose();
  }


  const [products, setProduct] = useState([]);
    const viewAllProduct=()=>{
       fetch(`http://localhost:4000/products/all`)
        .then(res => res.json())
        .then(data => {
            setProduct(data.map(product =>{

                return( 
                    <>
                      <tr>
                      <td>{product._id}</td>
                      <td>{product.name}</td>
                      <td>{product.description}</td>
                      <td>{product.price}</td>
                      <td>{product.stocks}</td>

                      <td>

                                
                          <div className= "btn-group-vertical">
                          {
                          (product.isActive)
                          ?
                          <Button variant="danger" onClick={() => disable(product._id, product.name)}>Disable</Button>
                          :
                          <>
                          <Button variant="primary" onClick={() => update(product._id, product.name)}>Update</Button>
                          <Button variant="success" onClick={() => enable(product._id, product.name)}>Enable</Button>
                          </>
                          }
                        </div>
                        
                      </td>
                      </tr>
                    </>
                )
            }))
        })
    }
    useEffect(() =>{
      viewAllProduct();
       
    },[])

    /*Disabling Product*/
    const disable = (productId, name) =>{
      console.log(productId);
      console.log(name);

      fetch(`http://localhost:4000/products/${productId}/archive`,{
        method: "PATCH",
        headers: {
            "Content-Type": "application/json",
            "Authorization": `Bearer ${localStorage.getItem('token')}`
        },
        body: JSON.stringify({
            isActive: false
        })
      
      })
      .then(res => res.json())
      .then(data =>{
        console.log(data);

        if(data){
          Swal.fire({
            title: "Product is now succesfully Disabled",
            icon: "success",
            text: `${name} is now disabled.`
          })
          viewAllProduct();
          
        }
        else{
          Swal.fire({
            title: "Disabling Product Unsuccessful!",
            icon: "error",
            text: `Something went wrong. Please try again later!`

          })
        }
      })

    }

    /*Enabling Product*/

    const enable = (productId, name) =>{
      console.log(productId);
      console.log(name);

      fetch(`http://localhost:4000/products/${productId}/archive`,{
        method: "PATCH",
        headers: {
            "Content-Type": "application/json",
            "Authorization": `Bearer ${localStorage.getItem('token')}`
        },
        body: JSON.stringify({
            isActive: true
        })
      
      })
      .then(res => res.json())
      .then(data =>{
        console.log(data);

        if(data){
          Swal.fire({
            title: "Product is now succesfully enabled",
            icon: "success",
            text: `${name} is now enabled.`
          })
          viewAllProduct();
        }
        else{
          Swal.fire({
            title: "Enabling Product is Unsuccessful!",
            icon: "error",
            text: `Something went wrong. Please try again later!`

          })
        }
      })
      
    }

  return (

    <>
     {/*Add New Products*/}
    <Modal show={show} onHide={handleClose} animation={false}>
       <Form onSubmit={(e) => addNewProduct(e)}>
        <Modal.Header closeButton>
          <Modal.Title>Add Product</Modal.Title>
        </Modal.Header>
        <Modal.Body>
       
                <Form.Group className="mb-3" controlId="formAddProductName" >
                  <Form.Label>Product Name</Form.Label>
                  <Form.Control type="product name"  onChange={e => setName(e.target.value)}/>
                </Form.Group>
                <Form.Group className="mb-3" controlId="formAddDescription">
                  <Form.Label>Description</Form.Label>
                  <Form.Control type="description"  onChange={e => setDescription(e.target.value)} />
                </Form.Group>
                <Form.Group className="mb-3" controlId="formAddPrice">
                  <Form.Label>Price</Form.Label>
                  <Form.Control type="price" onChange={e => setPrice(e.target.value)}/>
                </Form.Group>
                <Form.Group className="mb-3" controlId="formAddStocks">
                  <Form.Label>Stocks</Form.Label>
                  <Form.Control type="stocks" onChange={e => setStocks(e.target.value)}/>
                </Form.Group>
            
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleClose}>
            Close
          </Button>
          <Button variant="primary" type="submit" id="submitBtn">
                    Add
                  </Button>

        </Modal.Footer>
          </Form>
      </Modal>

        {/*Update Product*/}
      
      <Modal show={updateModal} onHide={updateClose} animation={false}>
         <Form onSubmit={(e) => updateProduct(e)}>
          <Modal.Header closeButton>
            <Modal.Title>Update Product</Modal.Title>
          </Modal.Header>
          <Modal.Body>
         
                  <Form.Group className="mb-3" controlId="name" >
                    <Form.Label>Product Name</Form.Label>
                    <Form.Control type="text" value = {name} onChange={e => setName(e.target.value)}/>
                  </Form.Group>
                  <Form.Group className="mb-3" controlId="description">
                    <Form.Label>Description</Form.Label>
                    <Form.Control type="text" value = {description} onChange={e => setDescription(e.target.value)}/>
                  </Form.Group>
                  <Form.Group className="mb-3" controlId="formUpdatePrice">
                    <Form.Label>Price</Form.Label>
                    <Form.Control type="number" value = {price} onChange={e => setPrice(e.target.value)}/>
                  </Form.Group>
                  <Form.Group className="mb-3" controlId="formUpdateStocks">
                    <Form.Label>Stocks</Form.Label>
                    <Form.Control type="number" value = {stocks} onChange={e => setStocks(e.target.value)}/>
                  </Form.Group>
              
          </Modal.Body>
          <Modal.Footer>
            <Button variant="primary" type="submit" id="submitBtn">
                      Update
                    </Button>
            <Button variant="secondary" onClick={updateClose}>
              Close
            </Button>

          </Modal.Footer>
            </Form>
        </Modal>


    <Row>
    <h1 className = "my-5 text-center">Admin Dashboard</h1></Row>
   <Row className="mb-3"> <div className = "text-center" >
      <Button variant="primary" onClick={handleShow}>Add New Product</Button>{' '}
      <Button variant="success">Show User Orders</Button>
    </div></Row>
    <br/>
    <Table striped bordered hover className="mt-3 text-center">
      <thead>
        <tr>
          <th>Product ID</th>
          <th>Product Name</th>
          <th>Description</th>
          <th>Price</th>
          <th>Stocks</th>
          <th>Actions</th>
        </tr>
      </thead>
      <tbody>
       {products}
      </tbody>
    </Table>
    </>
    

  );
}

