import { useState, useEffect, useContext } from "react";
import { Link, useParams, useNavigate } from "react-router-dom";


import Swal from "sweetalert2";
import { Container, Card, Button, Row, Col} from "react-bootstrap";

import UserContext from "../UserContext";

export default function ProductView(){

	
	const { user } = useContext(UserContext);
	const navigate = useNavigate();
	const { productId } = useParams();
	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState(0);
	const [stocks, setStocks] = useState(0);
	const [products, setProduct] = useState([]);
	

	const Checkout = (productId) => {
		fetch(`http://localhost:4000/products/product-details/${productId}`,{
			method: "GET",
			headers:{
				"Content-Type": "application/json",
				Authorization: `Bearer ${localStorage.getItem("token")}`
			},
			body: JSON.stringify({
				name: name,
      	description: description,
      	price: price
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

			if(data === true){
				Swal.fire({
					title: "Checkout successfully!",
					icon: "success",
				})
				navigate("/courses");
			}
			else{
				Swal.fire({
					title: "Something went wrong",
					icon: "error",
					text: "Please try again."
				})
			}
		})
	}
	
	useEffect(() =>{
		fetch(`http://localhost:4000/products/product-details/${productId}`)
        .then(res => res.json())
        .then(data => {
        	setName(data.name);
			setDescription(data.description);
			setPrice(data.price);  
        })

	}, [])

	
		// const [count, setCount] = useState(0);

		const incNum =()=>{
    if(stocks<10)
    {
    setStocks(Number(stocks)+1);
    }
  };
  const decNum = () => {
     if(stocks>0)
     {
      setStocks(stocks - 1);
     }
  }
 let handleChange = (e)=>{
   setStocks(e.target.value);
  }

	
	return(
		<Container className="mt-5">
			<Row>
				<Col lg>
					<Card>
					   <Card.Header className= "text-center"variant="secondary">{name}</Card.Header>
					    <Card.Body>
					      <Card.Text>
					       {description}
					       </Card.Text>
					       <Card.Text>
					         Price: Php {price}
					       </Card.Text>
					        Quantity:

					        <div className="col-xl-1">
					        <Card.Text>
					       {stocks}
					       </Card.Text>
					        <Button variant= "primary" size= "sm" onClick={(decNum)  => setStocks(stocks - 1)}>-</Button>
					        {' '}
					        <Button variant= "primary" size= "sm" onClick={(incNum) => setStocks(stocks + 1)}>+</Button>
					        </div>
					      </Card.Body>
					      <Card.Footer>
					      	<div className="d-grid gap-2">
					      		<Button variant="primary" size="lg">Checkout</Button>
					      	</div>
					      </Card.Footer>
					    </Card>
				</Col>
			</Row>
		</Container>
	)
}
